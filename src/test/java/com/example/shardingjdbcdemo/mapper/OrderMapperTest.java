package com.example.shardingjdbcdemo.mapper;

import com.example.shardingjdbcdemo.entity.TOrder;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.util.List;

/**
 * @author JiaZH
 * @date 2022/07/15
 */
@Slf4j
@SpringBootTest
public class OrderMapperTest {

    @Autowired
    TOrderMapper orderMapper;

    @Test
    void insert() {
        TOrder order = new TOrder();
        order.setName("豆腐");
        order.setUserId(3);
        orderMapper.insert(order);
        log.info("数据ID为：{}", order.getOrderId());
    }

    @Test
    void batchInsert() {
        for (int i = 0; i < 5000; i++) {
            TOrder order = new TOrder();
            order.setName("豆腐" + i);
            order.setUserId(i);
            orderMapper.insert(order);
            log.info("数据ID为：{}", order.getOrderId());
        }
    }

    @Test
    void selectById() {
        TOrder tOrder = orderMapper.selectById(754744279401758720L);
        log.info("查询数据为：{}", tOrder);
    }

    @Test
    void selectAll() {
        List<TOrder> tOrders = orderMapper.selectList(null);
        log.info("查询数据量为：{}", tOrders.size());
    }

}