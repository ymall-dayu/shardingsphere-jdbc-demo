package com.example.shardingjdbcdemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.shardingjdbcdemo.entity.TOrder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author Administrator
 * @description 针对表【t_order(订单表)】的数据库操作Mapper
 * @createDate 2022-07-15 16:11:27
 * @Entity com.example.shardingjdbcdemo.entity.TOrder
 */
@Mapper
public interface TOrderMapper extends BaseMapper<TOrder> {

    TOrder selectById(@Param("id") Long id);

}




