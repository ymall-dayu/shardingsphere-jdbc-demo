package com.example.shardingjdbcdemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.shardingjdbcdemo.entity.TOrderItem;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Administrator
 * @description 针对表【t_order_item(订单详细表)】的数据库操作Mapper
 * @createDate 2022-07-15 16:11:34
 * @Entity com.example.shardingjdbcdemo.entity.TOrderItem
 */
@Mapper
public interface TOrderItemMapper extends BaseMapper<TOrderItem> {

}




