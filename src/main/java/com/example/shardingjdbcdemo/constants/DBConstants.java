package com.example.shardingjdbcdemo.constants;

/**
 * @author JiaZH
 * @date 2022/07/15
 */
public interface DBConstants {

    String DATASOURCE_ORDERS = "ds-orders";

    String DATASOURCE_USERS = "ds-users";

}
