CREATE TABLE `t_order`
(
    `order_id` bigint NOT NULL COMMENT '主键',
    `name`     varchar(255) DEFAULT NULL COMMENT '名称',
    `user_id`  int          DEFAULT NULL COMMENT '用户ID',
    PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='订单表';


CREATE TABLE `t_order_item`
(
    `order_item_id` bigint NOT NULL COMMENT '主键',
    `order_id`      bigint       DEFAULT NULL COMMENT '订单ID',
    `name`          varchar(255) DEFAULT NULL COMMENT '名称',
    `user_id`       int          DEFAULT NULL COMMENT '用户ID',
    PRIMARY KEY (`order_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='订单详细表';